import express from "express";
import { registerMiddlewares } from "./routes/routes";

export const startServer = () => {
    const app = express();

    registerMiddlewares(app);
    const PORT = process.env.PORT;
    app.listen(PORT, () => {
        console.log(`SERVER STARTED LISTENING ON THE PORT ${PORT}`)
    })
}