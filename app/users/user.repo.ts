import { UserModel } from "./user.schema";
import { User } from "./user.type";

const get = () => UserModel.get();
const signIn = (user: User) => UserModel.signIn(user);
const logIn = (user: User) => UserModel.logIn(user);

export default {
   get, signIn, logIn
}