import { UserResponse } from "./user.type";

export const UserResponses: UserResponse = {
    USER_NOT_FOUND: {
        statusCode: 404,
        message: "USER NOT FOUND"
    },
    USER_ALREADY_EXIST: {
        statusCode: 200,
        message: "USER ALREADY EXIST"
    },
    USER_CREATED: {
        statusCode: 201,
        message: "USER CREATED"
    },
    USER_LOGGED_IN: {
        statusCode: 200,
        message: "USER LOGGED IN"
    }
}