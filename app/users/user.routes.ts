import { Router } from "express";
import { signIn, logIn, get } from "./user.services"
import { Route } from "../routes/routes.types";
import { ResponseHandler } from "../utility/response.handler";

const router = Router();

router.get('/users', (req, res, next) => {
    try {
        const result = get()
        res.send(new ResponseHandler(result))
    } catch (err: any) {
        next(err);
    }
})
router.post('/signin', (req, res, next) => {
    try {
        const result = signIn(req.body)
        res.send(new ResponseHandler(result))
    } catch (err: any) {
        next(err);
    }
})

router.post('/login', (req, res, next) => {
    try {
        const result = logIn(req.body)
        res.send(new ResponseHandler(result))
    } catch (err: any) {
        next(err);
    }
})

export default new Route('/user', router);
