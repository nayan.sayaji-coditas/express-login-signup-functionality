import { User } from "./user.type";

class UserSchema {
    private static users: User[] = [];

    get() {
        return [...UserSchema.users];
    }
    signIn(user: User) {
        const index = UserSchema.users.findIndex((u) => {
            return u.username === user.username && u.password === user.password;
        });

        if (index !== -1) return true;

        UserSchema.users.push(user);
    }
    logIn(user: User) {
        const index = UserSchema.users.findIndex((u) => {
            return u.username === user.username && u.password === user.password;
        });

        if (index === -1) return false;
        return true;
    }
}

export const UserModel = new UserSchema();