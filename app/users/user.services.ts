import UserRepository from "./user.repo";
import { User } from "./user.type";
import { UserResponses } from "./user.responses";


export const get = () => {
    return UserRepository.get()
}
export const signIn = (user: User) => {
    const isUserCreated = UserRepository.signIn(user);

    if (isUserCreated) throw UserResponses.USER_ALREADY_EXIST;

    return UserResponses.USER_CREATED;
}

export const logIn = (user: User) => {
    const isExist = UserRepository.logIn(user);

    if (!isExist) throw UserResponses.USER_NOT_FOUND;

    return UserResponses.USER_LOGGED_IN;

}